/* Calculadora con operaciones aritméticas
Suma
Resta
Multiplicación
División
*/

//Datos funciona como si fuera una base de datos
function datos() {
  let numerosIngresados = {
    num1: parseInt(document.querySelector("#Numero1").value),
    num2: parseInt(document.querySelector("#Numero2").value),
    resultado: document.querySelector("#resultado"),
  };

  return numerosIngresados;
}

function suma() {
  const numeros = datos();
  numeros.resultado.innerHTML = numeros.num1 + numeros.num2;
  return numeros.num1 + numeros.num2;
}

function resta() {
  const numeros = datos();
  numeros.resultado.innerHTML = numeros.num1 - numeros.num2;
  return numeros.num1 - numeros.num2;
}

function multiplicacion() {
  const numeros = datos();
  numeros.resultado.innerHTML = numeros.num1 * numeros.num2;
  return numeros.num1 * numeros.num2;
}

function division() {
  const numeros = datos();
  numeros.resultado.innerHTML = numeros.num1 / numeros.num2;
  return numeros.num1 / numeros.num2;
}

function botonFunciones() {
  document.querySelector("#sumaBtn").setAttribute("onclick", "suma()");
  document.querySelector("#restaBtn").setAttribute("onclick", "resta()");
  document
    .querySelector("#multBtn")
    .setAttribute("onclick", "multiplicacion()");
  document.querySelector("#divBtn").setAttribute("onclick", "division()");
}

function main() {
  botonFunciones();
}

main();
